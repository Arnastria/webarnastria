from django.db import models

# Create your models here.
class MessageModel(models.Model):
    name = models.CharField(max_length=100)
    message = models.CharField(max_length=300)
    time = models.TimeField()


    def __str__(self):
        return self.name