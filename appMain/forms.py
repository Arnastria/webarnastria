from django import forms
import datetime

class MessageForm (forms.Form):

    error_messages = {
        'required' : 'This field is required',
        'invalid' : 'This field is not valid'
    }

    attrs_form = {
        'class': 'form-control'
        }

    name = forms.CharField(label=None,required=True,max_length=10,
                               widget=forms.TextInput(attrs=attrs_form))

    message = forms.CharField(label=None,required=True, max_length=300,
                         widget=forms.Textarea(attrs=attrs_form))
