from django.urls import re_path
from .views import *


#url for app
urlpatterns = [
    re_path(r'^message-board/',view_message_board,name='message_board'),
    re_path(r'^profile/',view_profile,name='profile'),
    re_path(r'^message-submit/',submit_message,name='submit_message'),
    re_path('',view_home,name = 'redirect'),
    
]
