from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from .forms import *
import random

# Create your views here.
response = {}
def view_home(request):
    for key, value in request.session.items():
        print('{} => {}'.format(key, value))
    welcome_list = ['Selamat Datang!','Welcome!','ようこそ!','Bienvenue!','Willkommen!','Welkom!']
    welcome_message = random.choice(welcome_list)
    return render(request, 'home.html',{'welcome_message': welcome_message})

def view_message_board(request):
    if(MessageModel.objects.all().count() !=0):
        return render(request, 'message_board.html',{'Form' : MessageForm,'Message_list' : MessageModel.objects.all()})
    else:
        return render(request, 'message_board.html',{'Form' :MessageForm})

def submit_message(request):
    form = MessageForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "No Sender"
        response['message'] = request.POST['message'] if request.POST['message'] != "" else "No Message"
        response['time'] = datetime.datetime.now()
        status = MessageModel(name=response['name'],
                             message=response['message'],time = response['time'])

        status.save()
        return HttpResponseRedirect('/message-board/')
    else:
        return HttpResponseRedirect('/message-board/')

def view_profile(request):
    return render(request,'profile.html')
