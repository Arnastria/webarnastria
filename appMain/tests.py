from django.test import TestCase, Client,LiveServerTestCase
from datetime import datetime
from django.urls import resolve, reverse
from .views import *
from .models import MessageModel
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time



# Create your tests here.
class appMainUnitTest(TestCase):

    def setUp(self):
        return MessageModel.objects.create(name="Arnas",message="Pesan",time = datetime.datetime.now())

    def test_home_url_is_exist(self):
        response = Client().get('//')
        self.assertEqual(response.status_code,200)

    def test_home_view_is_correct(self):
        found = resolve('//')
        self.assertEqual(found.func,view_home)

    def test_home_after_delete_models(self):
        MessageModel.objects.all().delete()
        response = Client().get('/message-board/')
        self.assertEqual(response.status_code,200)

    def test_model_can_create(self):
        MessageModel.objects.all().delete()
        new_message = self.setUp()
        all_message = MessageModel.objects.all().count()
        self.assertEqual(all_message,1)

    def test_form_is_valid(self):
        form = MessageForm()
        self.assertIn('class="form-control', form.as_p())

    def test_form_validation_blank(self):
        form = MessageForm(data={'name':'','message':''})
        self.assertTrue(form.fields['message'].max_length == 300)
        self.assertFalse(form.is_valid())

    def test_model_content_is_valid(self):
        new_status = self.setUp()
        self.assertEqual(new_status.name, 'Arnas')
        self.assertEqual(new_status.message, 'Pesan')

    #test if the model can be printed
    def test_model_can_print(self):
        new_status = self.setUp()
        self.assertEqual('Arnas', new_status.__str__())

    def test_home_post_success_and_render_result(self):
        response_status = Client().post(reverse('submit_message'),
                                        {'name': 'Arnas', 'message': 'Pesan'})
        self.assertEqual(response_status.status_code, 302)

        response = Client().get('/message-board/')
        html_response = response.content.decode('utf8')
        self.assertIn('Arnas', html_response)

    def test_post_failure_and_render_result(self):
        response_status = Client().post(reverse('submit_message'), {'name': '', 'status': ''})
        self.assertEqual(response_status.status_code, 302)

        response = Client().get('/message-board/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Anonim', html_response)

class appMainUnitProfileTest(TestCase):
    def test_routing_url(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_view_profile(self):
        response = resolve('/profile/')
        self.assertEqual(response.func,view_profile)