from django.apps import AppConfig


class AppsubscribeConfig(AppConfig):
    name = 'appSubscribe'
