from django.urls import re_path,path
from django.conf.urls import url
from .views import *


#url for app
urlpatterns = [
    url(r'^emailValidation/$',validate_email),
    url(r'^passwordValidation/$',validate_password_delete),
    url(r'^registering/',register_subscriber),
    url(r'^seriaLizeModels/',seriaLizeModels),
    url(r'^deleteSubs/$',deleteSubs),
    path('',view_subscribe,name='subscribe-page'),
]
