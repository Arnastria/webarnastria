from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse 
from django.views.decorators.csrf import csrf_protect
import json
from .forms import registerSubscriberForm
from .models import subscriberModels
from django.core import serializers

response = {}
# Create your views here.
def view_subscribe(request):
    print("Subscrbve view")
    return render(request,"subscribe.html", {'Form' : registerSubscriberForm})

def validate_email(request):
    dataEmail = request.GET.get('email', None)
    print("validate_email"+dataEmail)
    response['isTaken'] = subscriberModels.objects.filter(email = dataEmail).exists()
    print(">>>","")
    print(subscriberModels.objects.filter(email = dataEmail).exists())

    return JsonResponse(response)


def register_subscriber(request):
    data = request.POST
    dataEmail = data.get('email',None)
    dataName = data.get('name',None)
    dataPassword = data.get('password',None)
    model = subscriberModels(email = dataEmail,name=dataName ,password=dataPassword).save()

    response['isSuccess'] = True
    return JsonResponse(response)

def seriaLizeModels(request):
    print("masuk serialize")
    models = subscriberModels.objects.values()
    data = list(models)

    return JsonResponse({'data':data})

def validate_password_delete(request):
    print("masuk validate password")
    data = request.GET
    dataEmail = data.get('email',None)
    dataPassword = data.get('password',None)
    object = subscriberModels.objects.get(email = dataEmail)
    response['isPassValid'] = (object.password == dataPassword)
    print("ini response")
    print(response['isPassValid'])

    return JsonResponse(response)

def deleteSubs(request):
    dataEmail = request.GET.get('email',None)
    isDeleted = False
    try :
        subscriberModels.objects.filter(email = dataEmail).delete()
        isDeleted = True
    except :
        isDeleted = False
    response['isDeleted'] = isDeleted

    return JsonResponse(response)