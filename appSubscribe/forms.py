from django import forms
from .models import subscriberModels

class registerSubscriberForm(forms.Form):
    attrs_form = {
        'class': 'form-control'
        }

    email = forms.EmailField(label='',required=True,
                            widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'Your E-Mail'}))

    name = forms.CharField(label='',required=True,
                            widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Your name'}))

    password = forms.CharField(label='',required = True,
                            widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': "Your Password"}))
    
    