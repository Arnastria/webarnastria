from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse,JsonResponse 
from .jikan_malAPI import *
import requests
import json

from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt


response = {}
# Create your views here.
def view_mal_list(request):
    print("--> MAL_LIST")
    for key, value in request.session.items():
        print('{} => {}'.format(key, value))
    if (request.user.is_authenticated) :
        print("user authenticated in view_mal_pages")
        if('favbookid' not in request.session.keys()):
            request.session['favbookid'] = []
            request.session['favbooknum'] = 0
    else:
        request.session['favbookid'] = []
        request.session['favbooknum'] = 0
    
    return render(request,"mal_pages.html")

def get_MAL_Query(request,title):
    print("--->MAL QUERY"+title)
    raw_data = get_search_manga(title)

    userdata = {}
    bookData = []
    
    if(request.user.is_authenticated):
        print("user authenticated when getting book")
        booksdata = request.session['favbookid']
        userdata['name'] = request.user.username
        userdata['favbookcounter'] = len(booksdata)
        
        # for book_id in booksdata:
        #     item = {}
        #     item['book_id'] = booksdata['book_id']
        #     bookData.append(item)
        userdata['favbookidlist'] = booksdata
    else :
        userdata['favbookcounter'] = 0
        userdata['favbookidlis'] = []

    data=[]
    for info in raw_data:
        item = {}
        item['id'] = info['mal_id']
        item['title'] = info['title']
        item['cover'] = info['image_url']
        item['authors'] = info['type']
        item['description'] = info['synopsis']
        data.append(item)

    return JsonResponse({"user_data":userdata,"data" : data})

def get_GBOOK_Query(request,title):
    print("-->GBOOK_Query "+title)
    raw_data = requests.get("https://www.googleapis.com/books/v1/volumes?q="+title).json()

    userdata = {}
    bookData = []
    
    if(request.user.is_authenticated):
        print("user authenticated when getting book")
        booksdata = request.session['favbookid']
        userdata['favbookcounter'] = len(booksdata)
        userdata['name'] = request.user.username
        
        # for book_id in booksdata:
        #     item = {}
        #     item['book_id'] = booksdata['book_id']
        #     bookData.append(item)
        userdata['favbookidlist'] = booksdata
    else :
        userdata['name'] = "guest"
        userdata['favbookcounter'] = 0
        userdata['favbookidlis'] = []

    data = []
    for info in raw_data['items'] :
        item = {}
        item['id'] = info['id']
        item['title'] = info['volumeInfo']['title']
        try :
            item['authors'] = ", ".join(info['volumeInfo']['authors'])
        except:
            item['authors'] = "not found"
        try:
            item['description'] = info['volumeInfo']['description'][:120]+"...."
        except :
             item['description'] = "not found"
        try :
            item['cover']=info['volumeInfo']['imageLinks']['thumbnail']
        except:
            item['cover']="https://files.catbox.moe/kh18gz.png"
        data.append(item)
    
    return JsonResponse({"user_data":userdata,"data" : data})

def get_Google_book(request):
    print("--> GOOGLE_BOOK")
    raw_data = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()

    userdata = {}
    bookData = []
    
    if(request.user.is_authenticated):
        print("user authenticated when getting book")
        booksdata = request.session['favbookid']
        userdata['favbookcounter'] = len(booksdata)
        
        for book_id in booksdata:
            item = {}
            item['book_id'] = booksdata['book_id']
            bookData.append(item)
        userdata['favbookidlist'] = booksdata
    else :
        userdata['favbookcounter'] = 0
        userdata['favbookidlis'] = []

    data = []
    for info in raw_data['items'] :
        item = {}
        item['id'] = info['id']
        item['title'] = info['volumeInfo']['title']
        item['authors'] = ", ".join(info['volumeInfo']['authors'])
        item['description'] = info['volumeInfo']['description'][:120]+"...."
        try :
            item['cover']=info['volumeInfo']['imageLinks']['thumbnail']
        except:
            item['cover']="https://files.catbox.moe/kh18gz.png"
        data.append(item)

    return JsonResponse({"user_data":userdata,"data" : data})

def logout(request):
    print("logout")
    auth.logout(request)
    request.session.flush()
    return HttpResponseRedirect('/mal-api/')

@csrf_exempt
def update_book(request):
    print("update book")
    raw_data = request.POST
    print("raw_data",end="")
    print(raw_data)
    type = raw_data.get("type",None)
    id = raw_data.get("book_id",None)

    if(type == "add"):
        print("add book")
        request.session['favbookid'].append(id)
        request.session['favbooknum'] = request.session['favbooknum']+1
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
    else :
        print("remove book")
        request.session['favbookid'].remove(id)
        request.session['favbooknum'] = request.session['favbooknum']-1
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
    
    return JsonResponse({"is success":True})

def call_counter(request):
    counter = request.session['favbooknum']

    return JsonResponse({"counter":counter})
