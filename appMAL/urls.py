from django.urls import re_path,path
from django.conf.urls import url
from .views import *


#url for app
urlpatterns = [
    path('get-books/',get_Google_book,name='get-google-books'),
    path('search/MAL/<str:title>/',get_MAL_Query),
    path('search/Gbooks/<str:title>/',get_GBOOK_Query),
    url('logout/',logout,name='logoutDjango'),
    url('updateBook/',update_book),
    url('callCounter/',call_counter),
    re_path('',view_mal_list,name = 'redirect_mal'),  
]
