import requests

SEASON_API = "https://api.jikan.moe/v3/season/"
TOP_MANGA_API = "https://api.jikan.moe/v3/top/manga/1"
TOP_ANIME_API = "https://api.jikan.moe/v3/top/anime/1"
MANGA_SEARCH_API = "https://api.jikan.moe/v3/search/manga/?q="
ANIME_SEARCH_API = "https://api.jikan.moe/v3/search/anime/?q="

def get_season(year=2018,season_string="summer"):
    season = requests.get(SEASON_API+str(year)+"/"+season_string)
    return season.json()["anime"]

def get_top_manga():
    manga = requests.get(TOP_MANGA_API)
    return manga.json()

def get_top_anime():
    anime = requests.get(TOP_ANIME_API)
    return anime.json()

def get_search_anime(name):
    result = requests.get(ANIME_SEARCH_API+name+"&limit=10")
    return result.json()["results"]

def get_search_manga(name):
    result = requests.get(MANGA_SEARCH_API+name+"&limit=10")
    return result.json()["results"]
