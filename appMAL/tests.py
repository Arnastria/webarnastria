from django.test import TestCase, Client,LiveServerTestCase
from datetime import datetime
from django.urls import resolve, reverse
from .views import *
from .jikan_malAPI import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class appMALunitTest(TestCase):
    
    def test_API_MAL_notNull(self):
        mal_get_season = get_season()
        mal_get_search_anime = get_search_anime("Grand Blue")
        mal_get_search_manga = get_search_manga("Grand Blue")
        mal_get_top_anime = get_top_anime()
        mal_get_top_manga = get_top_manga()

        self.assertEqual(True,mal_get_season is not None)
        self.assertEqual(True,mal_get_search_anime is not None)
        self.assertEqual(True,mal_get_search_manga is not None)
        self.assertEqual(True,mal_get_top_anime is not None)
        self.assertEqual(True,mal_get_top_manga is not None)

    def test_viewToMalPage(self):
        response = Client().get('/mal-api/')
        self.assertEqual(response.status_code,200)

    # def test_JsonResponseView_notNull(self):
    #     view_get_MAL_Query = Client().get('/mal-api/search/MAL/naruto/')
    #     view_get_Books = Client().get('/mal-api/get-books/')
    #     view_get_Books_Query = Client().get('/mal-api/search/Gbooks/React/')
        
    #     self.assertEqual(True, view_get_MAL_Query is not None)
    #     self.assertEqual(True, view_get_Books is not None)
    #     self.assertEqual(True, view_get_Books_Query is not None)
