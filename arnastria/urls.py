"""arnastria URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import re_path
from django.conf.urls import include,url

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^mal-api/',include('appMAL.urls'),name = "book"),
    re_path(r'^subscribe/',include('appSubscribe.urls'),name = "subscribe"),
    path('auth/',include('social_django.urls', namespace='social')),
    re_path('', include('appMain.urls')),
]
