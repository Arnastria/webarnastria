$(window).on('load', function () {
    $('#loading').fadeOut(5, function(){
      $('#bottom_div').show();
      $('#main_div').show();
    });
  });

  var nani=0;
  var onGBook = false;
  var onManga = false;
  var onAnime = false;

  $(document).ready(function(){     
    callCounter();
});


function setCounter(cnt){
    console.log("update counter with "+cnt)
    $("#counter").html(cnt);
}

function createInformationBooks(title,authors,description) {
    var res="";
    res+="<div class='wrapper_text_inside'>";
    res+="<h3>"+title+"</h3>";
    res+="<p><b>"+authors+"</b>";
    res+="<br>"+description+"</p>";
    res+="</div>"
    return res;
}

function createFav(id) { 
    return "<i id='star-"+id+"' class='far fa-star' onClick='FavMe(`"+id+"`)' style='font-size:45px'></i>";
 }

 function createFav2(id) { 
     return "<i id='star-"+id+"' class='fas fa-star' onClick='FavMe(`"+id+"`)' style='font-size:45px';color='rgb(249, 214, 34)'></i>";
  }

 function FavMe(id) { 
    id = id+""
    if("rgb(33, 37, 41)" === $("#star-"+id).css('color') ){
        $("#star-"+id).css('color','rgb(249, 214, 34)');
        $("#star-"+id).attr('class', 'fas fa-star');
        // nani++;
        // setCounter(nani);  
        ajaxUpdateBook("add",id)
    } else if("rgb(249, 214, 34)" === $("#star-"+id).css('color') ){
        $("#star-"+id).css('color','rgb(33, 37, 41)'); 
        $("#star-"+id).attr('class', 'far fa-star');
        // nani--;
        // setCounter(nani);
        ajaxUpdateBook("delete",id)
    }
    
  }

$('#btnStory').click(function(){
    console.log("click:btnStory");
    var title = $('#inputQuery').val();
    if(title === ""){
        alert("input can't be empty");
        return;
    }
    callAjax("search/Gbooks/"+title+"/");
    onGBook = false;
    onManga = false;
    onAnime = false;
})

$('#btnMAL').click(function(){
    console.log("click:btnMAL");
    var title = $('#inputQuery').val();
    if(title === ""){
        alert("input can't be empty");
        return;
    }
    callAjax("search/MAL/"+title+"/");
    onGBook = false;
    onManga = false;
    onAnime = false;
})

function setDataTable() { 
    console.log("DataTable");
    $('#table-result').DataTable();
 }

function callAjax(param) { 
    if(param === "get-books/" & onGBook){
        return;
    }
    $("#loading-table").show();
    $(".table-wrapper").hide();
    $.ajax({
        url:"/mal-api/"+param,
        dataType:"json",
        success: function (response) { 
            console.log("success");
            var user_data = response["user_data"]
            setCounter(user_data['favbookcounter'])
            var book_faved_list = user_data['favbookidlis']
            var lists = response["data"];
            var append = "";
            for (i = 0; i < lists.length; i++){
                append+="<tr>";
                append+="<td>"+(i+1)+"</td>";
                append+="<td>"+lists[i]['title']+"</td>";//kolom T A D
                append+="<td>"+lists[i]['authors']+"</td>";//kolom T A D 
                append+="<td style='text-align:justify'>"+lists[i]['description']+"</td>";//kolom T A D
                append+="<td>"+"<img class='cover-image' src="+lists[i]['cover']+">"+"</td>";//kolom cover
                var appended=false;
                for(var id in book_faved_list){
                    console.log("masuk sini")
                    console.log(id+":"+lists[i][id])
                    if( id == lists[i][id]){
                        console.log("ada yang sama")
                        append+="<td>"+createFav2(lists[i][id])+"</td>";
                        appended=true;
                        break;
                    }
                }
                if(!appended){
                    console.log("gak sama")
                    append+="<td>"+createFav(lists[i]['id'])+"</td>";//kolom kolom favorite   
                }
                append+="<tr>";
            }
            $('#result').html(append);    
            $("#loading-table").hide();
            $(".table-wrapper").show();
        }
    })
 }

function ajaxUpdateBook(type,params) {
    $.ajax({
        type:"POST",
        url: "/mal-api/updateBook/",
        data: {
            'type' : type,
            'book_id' : params,
        },
        dataType: "json",
        success: function (response) {
            console.log("success "+ type+" book with id "+params);
            callCounter();
        }
    });
     
 }

 function callCounter() { 
     $.ajax({
         url: "/mal-api/callCounter/",
         success: function (response) {
             var counter = response['counter']
             console.log("callCounter and get "+counter);
             setCounter(counter);
         }
     });
  }
 