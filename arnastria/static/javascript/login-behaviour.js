$(window).on('load', function () {
    $('#loading').fadeOut(5, function(){
      $('#bottom_div').show();
      $('#main_div').show();
    });
    updateTable();
  });

var emailValid = false;

function activateButton(){
  console.log("masuk activateButton");
  if($('#id_email').val() != '' & $('#id_name').val() != '' & $('#id_password').val() != '' & emailValid){
    $('#button_subscribe').prop('disabled',false);
  }
}

$('#id_email').change(function validateEmail(){
  console.log("masuk validate");
  var email = $('#id_email').val();
  
  if(email != '' & email.includes('@')){
     console.log('masuk ke ajax');
    $.ajax({
      url: "/subscribe/emailValidation/",
      data: {
        'email':email
      },
      dataType: "json",
      success: function (response) {
          if(response['isTaken']){
            $('.modal-body').html('Email '+email+' has been registered, please user another email.');
            $('#ModalEmail').modal('show');
            $('#button_subscribe').prop('disabled',true);
            emailValid = false;
          } else {
            emailValid = true
            activateButton();
          }  
      }
    });
  }
});

$('#id_name').change(function () { 
  activateButton();
 })

 $('#id_password').change(function () { 
  activateButton();
 })

$('#form_schedule').on('submit',function(param){
  param.preventDefault();
  var email = $('#id_email').val();
  var name = $('#id_name').val();
  var password = $('#id_password').val();
  var csrfToken = $('input[name=csrfmiddlewaretoken]').val()

  $.ajax({
    type: "POST",
    url: "/subscribe/registering/",
    data: {
      email: email,
      name : name,
      password : password,
      csrfmiddlewaretoken : csrfToken,
        },
    dataType: "json",
    success: function (response) {
      console.log('success register');
      $('#ModalEmailTitle').html('Congratulations');
      $('#modalBodySubscribe').html('You are registered, thank you !');
      $('#ModalEmail').modal('show');
      $('#button_subscribe').prop('disabled',true);
      emailValid = false;
      updateTable();
    }
  });
})
function createUns(email) { 
  return "<a  class='btn btn-danger unsub' onClick='popDeleteConfirm(`"+email+"`)' style = 'color:white'>Unsubscribe!</a>";
}

function popDeleteConfirm(email){
  $('#deleteConfirmP').html("Please type your password");
  $('#deleteConfirmP').css('color','black')
  $('#ModalDelete').modal('show');
  $('#deleteConfirmButton').click(function () { 
    var password = $('#deleteConfirmPassword').val();
    $.ajax({
      url: "/subscribe/passwordValidation/",
      data: {
        'email':email,
        'password':password
      },
      dataType: "json",
      success: function (response) {
        if(response['isPassValid']){
          console.log('password Valid');
          console.log(response['isPassValid']);
            deleteMe(email);
            $('#ModalDelete').modal('hide');
        }
        $('#deleteConfirmP').html("your password is wrong")
        $('#deleteConfirmP').css('color','red')
      }
    });
   })
}

function deleteMe(email){
  $.ajax({
    url: "/subscribe/deleteSubs/",
    data: {
      'email':email
    },
    dataType: "json",
    success: function (response) {
      if(response['isDeleted']){
        console.log("success delete");
        updateTable();
      }
      updateTable();
    }
  });
}
function updateTable(){
  $("#loading-table").show();
  $(".table-wrapper").hide();
  $.ajax({
    url:"/subscribe/seriaLizeModels/",
    dataType:"json",
    success: function (response) { 
        console.log("updateTable");
        var lists = response["data"];
        var append = "";
        for (i = 0; i < lists.length; i++){
            append+="<tr>";
            append+="<td style='width: 20%'>"+(i+1)+"</td>";
            append+="<td style='width: 40%'>"+lists[i]['name']+"</td>";//kolom T A D
            append+="<td style='text-align:right;width: 40%'>"+createUns(lists[i]['email'])+"</td>"
            append+="<tr>";
        }
        $('#result').html(append);    
        $("#loading-table").hide();
        $(".table-wrapper").show();
    }

})
}